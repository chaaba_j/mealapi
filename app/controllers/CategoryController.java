package controllers;

import controllers.authentification.BasicAuthentification;
import domains.User;
import models.CategoryModel;
import play.mvc.*;

/**
 * Created by chaabane on 26/12/13.
 */
public class CategoryController extends RestController
{
    public static Result    list()
    {
        return RestController.list(new CategoryModel(), null);
    }

    public static Result    show(long id)
    {
        return RestController.show(new CategoryModel(), null, id);
    }

    public static Result    add()
    {
        return RestController.add(new CategoryModel(),
                new BasicAuthentification(request().getHeader("Authorization"), User.USER));
    }

    public static Result    update()
    {
        return RestController.update(new CategoryModel(),
                new BasicAuthentification(request().getHeader("Authorization"), User.USER));
    }

    public static Result    delete(long id)
    {
        return RestController.delete(new CategoryModel(),
                new BasicAuthentification(request().getHeader("Authorization"), User.ADMIN), id);
    }
}
