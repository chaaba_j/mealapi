package controllers;

import controllers.authentification.BasicAuthentification;
import domains.User;
import exceptions.AuthentificationException;
import play.mvc.Controller;
import play.mvc.Result;
import utils.ErrorMessageFactory;
import utils.Serializer;

/**
 * Created by chaabane on 27/12/13.
 */
public class ChallengeController extends Controller
{
    public static Result    challenge()
    {
        User                    user;
        Serializer<User>        serializer = new Serializer<>();
        BasicAuthentification   auth;

        auth = new BasicAuthentification(request().getHeader("Authorization"), User.USER);
        try
        {
            user = auth.authentificate();
            return ok(serializer.serialize(user));
        }
        catch (AuthentificationException ex)
        {
            return forbidden(ErrorMessageFactory.create(Controller.FORBIDDEN, ex.getMessage()).serialize());
        }
    }
}
