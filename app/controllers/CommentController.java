package controllers;

import controllers.authentification.BasicAuthentification;
import domains.User;
import models.CommentModel;
import play.mvc.Controller;
import play.mvc.Result;

/**
 * Created by chaabane on 27/12/13.
 */
public class CommentController extends RestController
{
    public static Result    add(long mealId)
    {
        return RestController.add(new CommentModel(mealId),
                new BasicAuthentification(request().getHeader("Authorization"), User.USER));
    }

    public static Result    update()
    {
        return RestController.update(new CommentModel(),
                new BasicAuthentification(request().getHeader("Authorization"), User.USER));
    }

    public static Result    delete(long id)
    {
        return RestController.delete(new CommentModel(),
                new BasicAuthentification(request().getHeader("Authorization"), User.USER), id);
    }
}
