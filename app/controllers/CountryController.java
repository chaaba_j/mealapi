package controllers;

import controllers.authentification.BasicAuthentification;
import domains.User;
import models.CountryModel;
import play.mvc.*;

/**
 * Created by chaabane on 26/12/13.
 */
public class CountryController extends RestController
{
    public static Result    list()
    {
        return RestController.list(new CountryModel(), null);
    }

    public static Result    show(long id)
    {
        return RestController.show(new CountryModel(), null, id);
    }

    public static Result    add()
    {
        return RestController.add(new CountryModel(),
                new BasicAuthentification(request().getHeader("Authorization"), User.USER));
    }

    public static Result    update()
    {
        return RestController.update(new CountryModel(),
                new BasicAuthentification(request().getHeader("Authorization"), User.USER));
    }

    public static Result    delete(long id)
    {
        return RestController.delete(new CountryModel(),
                new BasicAuthentification(request().getHeader("Authorization"), User.ADMIN), id);
    }
}
