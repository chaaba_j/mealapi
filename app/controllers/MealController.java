package controllers;

import controllers.authentification.BasicAuthentification;
import domains.User;
import exceptions.ServerException;
import models.MealModel;
import play.mvc.*;


public class MealController extends RestController {

    protected interface ListFunction
    {
        String criteriaFunction() throws ServerException;
    }

    public static Result    list()
    {
        return RestController.list(new MealModel(), null);
    }

    public static Result    show(long id)
    {
        return RestController.show(new MealModel(), null, id);
    }

    public static Result    listByCategory(final long categoryId)
    {
        return RestController.listByCriteria(null, new RestController.ListFunction()
        {
            @Override
            public String search() throws ServerException
            {
                return (new MealModel()).searchByCategory(categoryId);
            }
        });
    }

    public static Result    listByRate(final int rate)
    {
        return RestController.listByCriteria(null, new RestController.ListFunction()
        {
            @Override
            public String search() throws ServerException
            {
                return (new MealModel()).searchByCategory(rate);
            }
        });
    }

    public static Result    listByCountry(final long countryId)
    {
        return RestController.listByCriteria(null, new RestController.ListFunction()
        {
            @Override
            public String search() throws ServerException
            {
                return (new MealModel()).searchByCategory(countryId);
            }
        });
    }

    public static Result    listByUser(final long userId)
    {
        return RestController.listByCriteria(null, new RestController.ListFunction()
        {
            @Override
            public String search() throws ServerException
            {
                return (new MealModel()).searchByUser(userId);
            }
        });
    }

    public static Result    add()
    {
        return RestController.add(new MealModel(),
                new BasicAuthentification(request().getHeader("Authorization"), User.USER));
    }

    public static Result    update()
    {
        return RestController.update(new MealModel(),
                new BasicAuthentification(request().getHeader("Authorization"), User.USER));
    }

    public static Result    delete(long id)
    {
        return RestController.delete(new MealModel(),
                new BasicAuthentification(request().getHeader("Authorization"), User.USER), id);
    }

}
