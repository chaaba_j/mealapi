package controllers;

import controllers.handlers.*;
import controllers.authentification.IAuthentification;
import domains.AbstractObject;
import exceptions.SerializableException;
import exceptions.ServerException;
import models.AbstractModel;
import models.MealModel;
import play.mvc.Controller;
import play.mvc.Result;
import utils.ErrorMessageFactory;

import java.util.Map;

/**
 * Created by chaabane on 27/12/13.
 */
public class RestController extends Controller
{
    private final static String MISSING_PARAMETER = "Missing obj parameter";

    protected static interface ListFunction
    {
        String search() throws ServerException;
    }

    protected static Result   listByCriteria(IAuthentification auth, ListFunction function)
    {
        try
        {
            return ok(function.search());
        }
        catch (ServerException ex)
        {
            return internalServerError(ErrorMessageFactory.create(INTERNAL_SERVER_ERROR, ex.getMessage()).serialize());
        }
    }

    protected static <T extends AbstractObject> Result list(AbstractModel<T> model,
                                                         IAuthentification auth)
    {
        final GetAllHandler<T> handler = new GetAllHandler<>(auth, model);

        try
        {
            return ok(handler.execute());
        }
        catch (SerializableException e)
        {
            return status(e.getErrorMsg().getType(), e.serialize());
        }
    }

    protected static <T extends AbstractObject> Result    show(AbstractModel<T> model,
                                                            IAuthentification auth,
                                                            long id)
    {
        final GetByIdHandler<T> handler = new GetByIdHandler<>(auth, model);

        try
        {
            return ok(handler.execute(id));
        }
        catch (SerializableException ex)
        {
            return status(ex.getErrorMsg().getType(), ex.serialize());
        }
    }

    private static String   getParam()
    {
        Map<String, String[]>   params = request().body().asFormUrlEncoded();

        if (params == null || params.containsKey("obj") == false)
        {
            return null;
        }
        return params.get("obj")[0];
    }

    protected static <T extends AbstractObject> Result    add(AbstractModel<T> model,
                                                              IAuthentification auth)
    {
        String                  jsonObj;
        final PostHandler<T>    handler = new PostHandler<>(auth, model);

        jsonObj = getParam();
        if (jsonObj == null)
        {
            return badRequest(ErrorMessageFactory.create(Controller.BAD_REQUEST, MISSING_PARAMETER).serialize());
        }
        try
        {
            return ok(handler.execute(jsonObj));
        }
        catch (SerializableException ex)
        {
            return status(ex.getErrorMsg().getType(), ex.getErrorMsg().serialize());
        }
    }

    protected static <T extends AbstractObject>  Result    update(AbstractModel<T> model,
                                                               IAuthentification auth)
    {
        String                jsonObj;
        final PutHandler<T>   handler = new PutHandler<>(auth, model);

        jsonObj = getParam();
        if (jsonObj == null)
        {
            return badRequest(ErrorMessageFactory.create(Controller.BAD_REQUEST, MISSING_PARAMETER).serialize());
        }
        try
        {
            return ok(handler.execute(jsonObj));
        }
        catch (SerializableException ex)
        {
            return status(ex.getErrorMsg().getType(), ex.getErrorMsg().serialize());
        }
    }

    protected static <T extends AbstractObject> Result    delete(AbstractModel<T> model,
                                                              IAuthentification auth,
                                                              long id)
    {
        final DeleteHandler<T>  handler = new DeleteHandler<>(auth, model);

        try
        {
            return ok(handler.execute(id));
        }
        catch (SerializableException ex)
        {
            return status(ex.getErrorMsg().getType(), ex.getErrorMsg().serialize());
        }
    }

}
