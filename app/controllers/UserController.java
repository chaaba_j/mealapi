package controllers;

import controllers.authentification.BasicAuthentification;
import domains.User;
import models.UserModel;
import play.mvc.*;

/**
 * Created by chaabane on 26/12/13.
 */
public class UserController extends RestController
{
    public static Result    list()
    {
        return list(new UserModel(), null);
    }

    public static Result    show(long id)
    {
        return show(new UserModel(), null, id);
    }

    public static Result    register()
    {
        return add(new UserModel(), null);
    }

    public static Result    update()
    {
        return update(new UserModel(),
                new BasicAuthentification(request().getHeader("Authorization"), User.USER));
    }

    public static Result    delete(long id)
    {
        return delete(new UserModel(),
                new BasicAuthentification(request().getHeader("Authorization"), User.ADMIN), id);
    }
}