package controllers.authentification;


import domains.User;
import exceptions.AuthentificationException;
import models.UserModel;
import org.apache.commons.codec.binary.Base64;
import utils.PasswordHashing;

/**
 * Created by chaabane on 27/12/13.
 */

public class BasicAuthentification implements IAuthentification
{
    private UserModel   model = new UserModel();
    private String      authorization;
    private int         userType;

    public BasicAuthentification(String authorization, int userType)
    {
        this.authorization = authorization;
        this.userType = userType;
    }

    @Override
    public User authentificate() throws AuthentificationException
    {
        User            user;
        String          decodedString;
        String          username;
        String          password;

        if (authorization == null)
        {
            throw new AuthentificationException("Authentification required");
        }
        decodedString  = new String(Base64.decodeBase64(authorization.split(" ")[1].getBytes()));
        username = decodedString.split(":")[0];
        password = decodedString.split(":")[1];
        user = model.getUser(username, PasswordHashing.hashWithSalt(password));
        if (user == null || user.getUserType() < userType)
        {
            throw new AuthentificationException("Access forbidden");
        }
        return user;
    }
}
