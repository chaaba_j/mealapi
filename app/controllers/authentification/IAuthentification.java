package controllers.authentification;

import domains.User;
import exceptions.AuthentificationException;

/**
 * Created by chaabane on 27/12/13.
 */
public interface IAuthentification
{
    public User authentificate() throws AuthentificationException;
}
