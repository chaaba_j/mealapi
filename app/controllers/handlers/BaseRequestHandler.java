package controllers.handlers;

import controllers.authentification.IAuthentification;
import domains.AbstractObject;
import domains.User;
import exceptions.AuthentificationException;
import models.AbstractModel;

/**
 * Created by chaabane on 27/12/13.
 */
public class BaseRequestHandler<T extends AbstractObject>
{
    protected IAuthentification auth;
    protected AbstractModel<T> model;

    public BaseRequestHandler(AbstractModel<T> model)
    {
        this.model = model;
    }

    public BaseRequestHandler(IAuthentification auth, AbstractModel<T> model)
    {
        this.auth = auth;
        this.model = model;
    }

    protected User checkAuthentification() throws AuthentificationException
    {
        if (auth != null)
        {
            return auth.authentificate();
        }
        return null;
    }
}
