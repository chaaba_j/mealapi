package controllers.handlers;

import controllers.authentification.IAuthentification;
import domains.AbstractObject;
import domains.User;
import exceptions.*;
import models.AbstractModel;

/**
 * Created by chaabane on 27/12/13.
 */
public class DeleteHandler<T extends AbstractObject> extends BaseRequestHandler<T>
{
    public DeleteHandler(AbstractModel model)
    {
        super(model);
    }

    public DeleteHandler(IAuthentification auth, AbstractModel<T> model)
    {
        super(auth, model);
    }

    public String execute(long id) throws SerializableException
    {
        User user = this.checkAuthentification();

        return model.delete(user, id);
    }
}
