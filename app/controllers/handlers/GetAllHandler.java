package controllers.handlers;

import controllers.authentification.IAuthentification;
import domains.AbstractObject;
import exceptions.SerializableException;
import models.AbstractModel;

/**
 * Created by chaabane on 27/12/13.
 */
public class GetAllHandler<T extends AbstractObject> extends BaseRequestHandler<T>
{
    public GetAllHandler(AbstractModel<T> model)
    {
        super(model);
    }

    public GetAllHandler(IAuthentification auth, AbstractModel model)
    {
        super(auth, model);
    }

    public String execute() throws SerializableException
    {
        this.checkAuthentification();
        return model.getAll();
    }

}
