package controllers.handlers;

import controllers.authentification.IAuthentification;
import domains.AbstractObject;
import exceptions.SerializableException;
import models.AbstractModel;

/**
 * Created by chaabane on 27/12/13.
 */
public class GetByIdHandler<T extends AbstractObject> extends BaseRequestHandler<T>
{
    public GetByIdHandler(AbstractModel<T> model)
    {
        super(model);
    }

    public GetByIdHandler(IAuthentification auth, AbstractModel<T> model)
    {
        super(auth, model);
    }

    public String  execute(long id) throws SerializableException
    {
        this.checkAuthentification();
        return model.getById(id);

    }
}
