package controllers.handlers;

import controllers.authentification.IAuthentification;
import domains.AbstractObject;
import domains.User;
import exceptions.SerializableException;
import models.AbstractModel;


/**
 * Created by chaabane on 27/12/13.
 */
public class PostHandler<T extends AbstractObject> extends BaseRequestHandler<T>
{
    public PostHandler(AbstractModel model)
    {
        super(model);
    }

    public PostHandler(IAuthentification auth, AbstractModel model)
    {
        super(auth, model);
    }


    public String execute(String param) throws SerializableException
    {
        User user = this.checkAuthentification();
        return model.add(user, param);
    }
}
