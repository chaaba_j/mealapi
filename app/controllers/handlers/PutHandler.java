package controllers.handlers;

import controllers.authentification.IAuthentification;
import domains.AbstractObject;
import domains.User;
import exceptions.*;
import models.AbstractModel;

/**
 * Created by chaabane on 27/12/13.
 */
public class PutHandler<T extends AbstractObject> extends BaseRequestHandler<T>
{
    public PutHandler(AbstractModel model)
    {
        super(model);
    }

    public PutHandler(IAuthentification auth, AbstractModel model)
    {
        super(auth, model);
    }


    public String   execute(String param) throws SerializableException
    {
        User user = this.checkAuthentification();

        return model.update(user, param);
    }
}
