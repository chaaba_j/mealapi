package dao;

import epitech.orm.exception.DORMException;
import epitech.orm.query.Restriction;
import epitech.orm.session.Session;
import exceptions.ServerException;
import utils.DatabaseOperation;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by chaabane on 26/12/13.
 */
public class BaseDAO
{
    public static <T, U> List<T>   searchByCriteria(final Class<T> clazz, final String criteriaName, final U value) throws ServerException
    {
        DatabaseOperation<List<T> > dbOperation =  new DatabaseOperation<>();

        return dbOperation.get(new DatabaseOperation.Operation<List<T>>()
        {
            @Override
            public List<T> tryIt(Session session) throws DORMException, SQLException
            {
                return (List<T>) session.select(clazz)
                        .where(Restriction.equal(criteriaName, value)).execute().list();
            }
        });
    }

    public static <T> List<T>    getAll(final Class<T> clazz) throws ServerException
    {
        DatabaseOperation<List<T> > dbOperation = new DatabaseOperation<>();

        return dbOperation.get(new DatabaseOperation.Operation<List<T>>()
        {
            @Override
            public List<T> tryIt(Session session) throws DORMException, SQLException
            {
                return (List<T>) session.select(clazz).execute().list();
            }
        });
    }

    public static <T> T         getById(final Class<T> clazz, final long id) throws ServerException
    {
        DatabaseOperation<T> dbOperation = new DatabaseOperation<>();

        return dbOperation.get(new DatabaseOperation.Operation<T>()
        {
            @Override
            public T tryIt(Session session) throws DORMException, SQLException
            {
                return (T) session.select(clazz).where(Restriction.equal("id", id)).execute().unique();
            }
        });
    }


    public static <T> T      save(final T obj) throws ServerException
    {
        DatabaseOperation<T>    dbOperation = new DatabaseOperation<>();

        return dbOperation.transaction(new DatabaseOperation.TransactionOperation<T>()
        {
            @Override
            public T tryIt(Session session) throws SQLException
            {
                return session.save(obj);
            }
        });
    }

    public static <T> T      delete(final T obj) throws ServerException
    {
        DatabaseOperation<T>    dbOperation = new DatabaseOperation<>();

        return dbOperation.transaction(new DatabaseOperation.TransactionOperation<T>()
        {
            @Override
            public T tryIt(Session session) throws SQLException
            {
                session.delete(obj);
                return obj;
            }
        });
    }

}
