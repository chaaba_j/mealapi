package dao;

import domains.Comment;
import epitech.orm.exception.DORMException;
import epitech.orm.query.Restriction;
import epitech.orm.session.Session;
import exceptions.ServerException;
import utils.DatabaseOperation;

import java.sql.SQLException;

/**
 * Created by chaabane on 30/12/13.
 */
public class CommentDAO extends BaseDAO
{
    public static Comment   getLastCommentByMeal(final long idMeal) throws ServerException
    {
        DatabaseOperation<Comment>  operation = new DatabaseOperation<>();

        return operation.get(new DatabaseOperation.Operation<Comment>()
        {
            @Override
            public Comment tryIt(Session session) throws DORMException, SQLException
            {
                return (Comment) session.select(Comment.class).where(Restriction.equal("meal_id", idMeal))
                        .last().execute().unique();
            }
        });
    }
}
