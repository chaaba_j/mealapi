package dao;

import domains.Meal;
import epitech.orm.exception.DORMException;
import epitech.orm.query.Restriction;
import epitech.orm.session.Session;
import exceptions.ServerException;
import play.Logger;
import utils.DatabaseOperation;
import utils.SessionSingleton;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by chaabane on 26/12/13.
 */
public class MealDAO extends BaseDAO
{
    public static List<Meal>    searchByCategory(long mealId) throws ServerException
    {
        return searchByCriteria(Meal.class, "category_id", mealId);
    }

    public static List<Meal>    searchByRate(int rate) throws ServerException
    {
        return searchByCriteria(Meal.class, "rate", rate);
    }

    public static List<Meal>    searchByCountry(long countryId) throws ServerException
    {
        return searchByCriteria(Meal.class, "country_id", countryId);
    }

    public static List<Meal>    searchByUser(long userId) throws ServerException
    {
        return searchByCriteria(Meal.class, "user_id", userId);
    }
}
