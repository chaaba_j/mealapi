package dao;

import domains.User;
import epitech.orm.exception.DORMException;
import epitech.orm.query.Restriction;
import epitech.orm.session.Session;
import exceptions.ServerException;
import play.Logger;
import utils.DatabaseOperation;
import utils.SessionSingleton;

import java.sql.SQLException;

/**
 * Created by chaabane on 26/12/13.
 */
public class UserDAO extends BaseDAO
{
    public static User getUser(final String username, final String password) throws ServerException
    {
        DatabaseOperation<User> dbOperation = new DatabaseOperation<>();

        return dbOperation.get(new DatabaseOperation.Operation<User>()
        {
            @Override
            public User tryIt(Session session) throws DORMException, SQLException
            {
                return (User) session.select(User.class).where(Restriction.equal("username", username))
                        .and(Restriction.equal("password", password)).execute().unique();
            }
        });
    }

    public static User getUserByName(final String username) throws ServerException
    {
        DatabaseOperation<User> dbOperation = new DatabaseOperation<>();

        return dbOperation.get(new DatabaseOperation.Operation<User>()
        {
            @Override
            public User tryIt(Session session) throws DORMException, SQLException
            {
                return (User) session.select(User.class).where(Restriction.equal("username", username))
                        .execute().unique();
            }
        });
    }
}
