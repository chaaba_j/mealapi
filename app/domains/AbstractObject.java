package domains;

import com.google.gson.annotations.Expose;

public abstract class AbstractObject
{
    @Expose
    protected int id;

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

}
