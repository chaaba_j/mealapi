package domains;

import com.google.gson.annotations.Expose;

/**
 * Created by chaabane on 25/12/13.
 */
public class Comment extends AbstractObject
{
    @Expose
    private User author;
    @Expose
    private String  comment;


    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
