package domains;

import com.google.gson.annotations.Expose;

import java.util.List;

public class Meal extends AbstractObject
{
    public static final int VERY_BAD = 0;
    public static final int BAD = 1;
    public static final int AVERAGE = 2;
    public static final int GOOD = 3;
    public static final int VERY_GOOD = 4;

    @Expose
    private String          name;
    @Expose
    private int             rate;
    @Expose
    private String          description;
    @Expose
    private String          imageUrl;
    @Expose
    private MealCategory    category;
    @Expose
    private MealCountry     country;
    @Expose
    private User            poster;
    @Expose
    private List<Comment>   comments;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getRate() {
        return rate;
    }

    public void setRate(int rate) {
        this.rate = rate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public MealCategory getCategory() {
        return category;
    }

    public void setCategory(MealCategory category) {
        this.category = category;
    }

    public MealCountry getCountry() {
        return country;
    }

    public void setCountry(MealCountry country) {
        this.country = country;
    }

    public User getPoster() {
        return poster;
    }

    public void setPoster(User poster) {
        this.poster = poster;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }
}
