package domains;

import com.google.gson.annotations.Expose;

/**
 * Created by chaabane on 24/12/13.
 */
public class MealCategory extends AbstractObject
{
    @Expose
    private String  name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
