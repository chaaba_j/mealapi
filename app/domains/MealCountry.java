package domains;

import com.google.gson.annotations.Expose;

/**
 * Created by chaabane on 24/12/13.
 */
public class MealCountry extends AbstractObject
{
    @Expose
    private String  name;
    @Expose
    private String  mealDescription;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMealDescription() {
        return mealDescription;
    }

    public void setMealDescription(String mealDescription) {
        this.mealDescription = mealDescription;
    }
}
