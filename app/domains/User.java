package domains;

import com.google.gson.annotations.Expose;

/**
 * Created by chaabane on 24/12/13.
 */
public class User extends AbstractObject
{
    public static final int USER = 0;
    public static final int ADMIN = 1;

    @Expose
    private String  username;
    private String  password;
    @Expose
    private int     userType;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


    public int getUserType() {
        return userType;
    }

    public void setUserType(int userType) {
        this.userType = userType;
    }
}
