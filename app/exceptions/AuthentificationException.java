package exceptions;

/**
 * Created by chaabane on 27/12/13.
 */
public class AuthentificationException extends SerializableException
{

    public AuthentificationException(String message)
    {
        super(FORBIDDEN, message);
    }
}
