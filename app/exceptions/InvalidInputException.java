package exceptions;

/**
 * Created by chaabane on 27/12/13.
 */
public class InvalidInputException extends SerializableException
{
    public InvalidInputException(String message)
    {
        super(MALFORMED, message);
    }
}
