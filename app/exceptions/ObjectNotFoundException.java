package exceptions;

/**
 * Created by chaabane on 27/12/13.
 */
public class ObjectNotFoundException extends SerializableException
{
    public ObjectNotFoundException(String message)
    {
        super(NOT_FOUND, message);
    }
}
