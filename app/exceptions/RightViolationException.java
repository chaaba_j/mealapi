package exceptions;

/**
 * Created by chaabane on 27/12/13.
 */
public class RightViolationException extends SerializableException
{
    public RightViolationException(String message)
    {
        super(FORBIDDEN, message);
    }
}
