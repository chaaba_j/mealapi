package exceptions;

import utils.ErrorMessage;

/**
 * Created by chaabane on 27/12/13.
 */
public class SerializableException extends RuntimeException
{
    private ErrorMessage    errorMsg;

    public static final int FORBIDDEN = 403;
    public static final int MALFORMED = 400;
    public static final int NOT_FOUND = 404;
    public static final int SERVER_ERROR = 500;

    protected SerializableException()
    {

    }

    public SerializableException(int type, String message)
    {
        this.errorMsg = new ErrorMessage(type, message);
    }

    public String   serialize()
    {
        return getErrorMsg().serialize();
    }


    public ErrorMessage getErrorMsg()
    {
        return errorMsg;
    }
}
