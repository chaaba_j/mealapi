package exceptions;

/**
 * Created by chaabane on 28/12/13.
 */
public class ServerException extends SerializableException
{
    public ServerException(String message)
    {
        super(SERVER_ERROR, message);
    }
}
