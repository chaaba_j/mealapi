package models;

import dao.BaseDAO;
import domains.AbstractObject;
import domains.User;
import exceptions.InvalidInputException;
import exceptions.ObjectNotFoundException;
import exceptions.RightViolationException;
import utils.Deserializer;
import utils.Serializer;

import java.util.List;

/**
 * Created by chaabane on 26/12/13.
 */
public abstract class AbstractModel<T extends AbstractObject>
{
    protected Class<T>      clazz;

    protected abstract void     onAdd(User user, T obj) throws InvalidInputException;
    protected abstract void     onUpdate(User user, T dbObj, T obj) throws InvalidInputException, RightViolationException;
    protected abstract void     onDelete(User user, T obj) throws RightViolationException;
    protected abstract boolean  checkObject(T obj);


    protected <U> boolean   exists(String name, U value)
    {
        return BaseDAO.searchByCriteria(clazz, name, value).size() > 0;
    }

    protected boolean   exists(T obj)
    {
        return BaseDAO.getById(clazz, obj.getId()) != null;
    }

    public String  getAll()
    {
        Serializer<List<T> >   serializer = new Serializer<>();

        return serializer.serialize(BaseDAO.getAll(clazz));
    }

    public String     getById(long id) throws ObjectNotFoundException
    {
        T               obj;
        Serializer<T>   serializer = new Serializer<>();

        obj = BaseDAO.getById(clazz, id);
        if (obj == null)
        {
            throw new ObjectNotFoundException("Object not found");
        }
        return serializer.serialize(obj);
    }

    public String    add(User user, String json) throws InvalidInputException
    {
        T               obj;
        Serializer<T>   serializer = new Serializer<>();
        Deserializer<T> deserializer = new Deserializer<>();

        obj = deserializer.deserialize(json, clazz);
        if (obj == null || !this.checkObject(obj))
        {
            throw new InvalidInputException("Object is invalid");
        }
        this.onAdd(user, obj);
        return serializer.serialize(BaseDAO.save(obj));
    }

    public String    update(User user, String json) throws InvalidInputException, ObjectNotFoundException
    {
        T               obj;
        T               dbObj;
        Serializer<T>   serializer = new Serializer<>();
        Deserializer<T> deserializer = new Deserializer<>();

        obj = deserializer.deserialize(json, clazz);
        if (obj == null || !this.checkObject(obj) || obj.getId() == 0)
        {
            throw new InvalidInputException("Invalid input");
        }
        dbObj = BaseDAO.getById(clazz, obj.getId());
        if (dbObj == null)
        {
            throw new ObjectNotFoundException("Item not found");
        }
        this.onUpdate(user, dbObj, obj);
        return serializer.serialize(BaseDAO.save(obj));
    }

    public String     delete(User user, long id) throws ObjectNotFoundException, RightViolationException
    {
        Serializer<T>   serializer = new Serializer<>();
        T               obj;

        obj = BaseDAO.getById(clazz, id);
        if (obj == null)
        {
            throw new ObjectNotFoundException("Object not found");
        }
        this.onDelete(user, obj);
        return serializer.serialize(BaseDAO.delete(obj));
    }
}
