package models;

import domains.MealCategory;
import domains.User;
import exceptions.InvalidInputException;
import exceptions.RightViolationException;
import utils.RegexUtils;

/**
 * Created by chaabane on 27/12/13.
 */
public class CategoryModel extends AbstractModel<MealCategory>
{
    public CategoryModel()
    {
        this.clazz = MealCategory.class;
    }

    @Override
    protected void     onAdd(User user, MealCategory obj) throws InvalidInputException
    {
        if (this.exists("name", obj.getName()))
            throw new InvalidInputException("Duplicate category");
    }

    @Override
    protected void     onUpdate(User user, MealCategory dbObj, MealCategory obj) throws InvalidInputException, RightViolationException
    {
    }

    @Override
    protected void     onDelete(User user, MealCategory obj) throws RightViolationException
    {
    }

    @Override
    protected boolean  checkObject(MealCategory category)
    {
        return category != null && RegexUtils.isValidName(category.getName());
    }
}
