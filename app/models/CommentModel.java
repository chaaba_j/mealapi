package models;


import dao.CommentDAO;
import dao.MealDAO;
import domains.Comment;
import domains.Meal;
import domains.User;
import exceptions.InvalidInputException;
import exceptions.ObjectNotFoundException;
import exceptions.RightViolationException;
import utils.Deserializer;
import utils.Serializer;

/**
 * Created by chaabane on 27/12/13.
 */
public class CommentModel extends AbstractModel<Comment>
{
    private long        mealId;

    public CommentModel(long mealId)
    {
        this.mealId = mealId;
        this.clazz = Comment.class;
    }

    public CommentModel()
    {
        this.clazz = Comment.class;
    }

    @Override
    public String       add(User user, String json) throws InvalidInputException
    {
        Deserializer<Comment>   deserializer = new Deserializer<>();
        Serializer<Comment>     serializer = new Serializer<>();
        Comment                 comment;
        Meal                    meal;

        comment = deserializer.deserialize(json, clazz);
        if (comment == null)
        {
            throw new InvalidInputException("Invalid input");
        }
        meal = MealDAO.getById(Meal.class, mealId);
        if (meal == null)
        {
            throw new ObjectNotFoundException("Cannot find meal for id : " + mealId);
        }
        meal.getComments().add(comment);
        MealDAO.save(meal);
        return serializer.serialize(CommentDAO.getLastCommentByMeal(mealId));
    }

    @Override
    protected void     onAdd(User user, Comment obj) throws InvalidInputException
    {
        obj.setAuthor(user);
    }

    @Override
    protected void     onUpdate(User user, Comment dbObj, Comment obj) throws InvalidInputException, RightViolationException
    {
        if (dbObj.getAuthor().getId() != obj.getAuthor().getId() && user.getUserType() != User.ADMIN)
        {
            throw new RightViolationException("Cannot change comment owner");
        }
    }

    @Override
    protected void     onDelete(User user, Comment obj) throws RightViolationException
    {
        if (user.getId() != obj.getAuthor().getId() && user.getUserType() != User.ADMIN)
        {
            throw new RightViolationException("Cannot delete user comment");
        }
    }

    @Override
    protected boolean  checkObject(Comment comment)
    {
        return true;
    }
}
