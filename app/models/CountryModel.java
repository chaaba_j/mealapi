package models;

import domains.MealCountry;
import domains.User;
import exceptions.InvalidInputException;
import exceptions.RightViolationException;
import utils.RegexUtils;

/**
 * Created by chaabane on 27/12/13.
 */
public class CountryModel extends AbstractModel<MealCountry>
{
    public CountryModel()
    {
        this.clazz = MealCountry.class;
    }

    @Override
    protected void     onAdd(User user, MealCountry obj) throws InvalidInputException
    {
        if (this.exists("name", obj.getName()))
            throw new InvalidInputException("Duplicate country");
    }

    @Override
    protected void     onUpdate(User user, MealCountry dbObj, MealCountry obj) throws InvalidInputException, RightViolationException
    {
    }

    @Override
    protected void     onDelete(User user, MealCountry obj) throws RightViolationException
    {
    }

    @Override
    protected boolean  checkObject(MealCountry country)
    {
        return country != null && RegexUtils.isValidName(country.getName());
    }
}
