package models;

import dao.MealDAO;
import domains.Comment;
import domains.Meal;
import domains.User;
import exceptions.InvalidInputException;
import exceptions.RightViolationException;
import utils.RegexUtils;
import utils.Serializer;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by chaabane on 27/12/13.
 */
public class MealModel extends AbstractModel<Meal>
{
    public MealModel()
    {
        this.clazz = Meal.class;
    }

    @Override
    protected void     onAdd(User user, Meal obj) throws InvalidInputException
    {
        obj.setPoster(user);
        obj.setRate(0);
        obj.setComments(new ArrayList<Comment>());
    }

    @Override
    protected void     onUpdate(User user, Meal dbObj, Meal obj) throws InvalidInputException, RightViolationException
    {
        if (obj.getPoster().getId() != user.getId() && user.getUserType() != User.ADMIN)
        {
            throw new RightViolationException("Only admin can change poster meal");
        }
        else if (user.getId() == obj.getPoster().getId() && dbObj.getRate() != obj.getRate())
        {
            throw new RightViolationException("Only other users can rate your meal");
        }
        else if (dbObj.getComments().size() != obj.getComments().size())
        {
            throw new RightViolationException("You cannot remove comment with this service use comment service");
        }
    }

    @Override
    protected void     onDelete(User user, Meal obj) throws RightViolationException
    {
        if (user.getId() != obj.getPoster().getId() && user.getUserType() != User.ADMIN)
        {
            throw new RightViolationException("Only admin can delete meal from other users");
        }
    }

    @Override
    protected boolean  checkObject(Meal meal)
    {
        if (meal.getName() != null && meal.getImageUrl() != null)
            return RegexUtils.isValidName(meal.getName()) && RegexUtils.isValidUrl(meal.getImageUrl());
        return false;
    }

    public String   searchByCategory(long categoryId)
    {
        Serializer<List<Meal> > serializer = new Serializer<>();

        return serializer.serialize(MealDAO.searchByCategory(categoryId));
    }

    public String   searchByRate(int rate)
    {
        Serializer<List<Meal> > serializer = new Serializer<>();

        return serializer.serialize(MealDAO.searchByRate(rate));
    }

    public String   searchByCountry(long countryId)
    {
        Serializer<List<Meal> > serializer = new Serializer<>();

        return serializer.serialize(MealDAO.searchByCountry(countryId));
    }

    public String   searchByUser(long userId)
    {
        Serializer<List<Meal> > serializer = new Serializer<>();

        return serializer.serialize(MealDAO.searchByUser(userId));
    }
}
