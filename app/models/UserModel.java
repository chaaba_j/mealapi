package models;

import dao.UserDAO;
import domains.User;
import exceptions.InvalidInputException;
import exceptions.RightViolationException;
import utils.PasswordHashing;
import utils.RegexUtils;

/**
 * Created by chaabane on 27/12/13.
 */
public class UserModel extends AbstractModel<User>
{
    private static int PASSWORD_MIN_SIZE = 6;

    public UserModel()
    {
        this.clazz = User.class;
    }

    @Override
    protected  void     onAdd(User authUser, User user) throws InvalidInputException
    {
        User            dbUser = UserDAO.getUserByName(user.getUsername());

        if (this.exists("username", user.getUsername()))
            throw new InvalidInputException("User exists please use another username");
        if (user.getPassword().length() < PASSWORD_MIN_SIZE)
            throw new InvalidInputException("password must have least 6 character");
        user.setPassword(PasswordHashing.createHash(user.getPassword()));
        user.setUserType(User.USER);
    }

    protected void      onUpdate(User authUser, User dbUser, User user) throws InvalidInputException, RightViolationException
    {
        user.setPassword(dbUser.getPassword());
        if (user.getUserType() == User.ADMIN && dbUser.getUserType() == User.USER)
        {
            throw new RightViolationException("Cannot pass user to admin");
        }
    }

    @Override
    protected void     onDelete(User authUser, User user) throws RightViolationException
    {
        if (authUser.getId() != user.getId() && user.getUserType() != User.ADMIN)
        {
            throw new RightViolationException("Cannot delete other users");
        }
    }

    @Override
    protected boolean  checkObject(User user)
    {
        return RegexUtils.isValidName(user.getUsername());
    }

    public User getUser(String username, String password)
    {
        return UserDAO.getUser(username, password);
    }
}
