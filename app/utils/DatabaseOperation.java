package utils;

import domains.AbstractObject;
import epitech.orm.exception.DORMException;
import epitech.orm.session.Session;
import epitech.orm.session.Transaction;
import exceptions.ServerException;
import play.Logger;

import java.sql.SQLException;

/**
 * Created by chaabane on 29/12/13.
 */
public class DatabaseOperation<T>
{
    public final static String  DATABASE_ERROR = "An error occured please inform administrator";



    public interface Operation<T>
    {
        public T        tryIt(Session session) throws DORMException, SQLException;
    }

    public interface TransactionOperation<T>
    {
        public T tryIt(Session session) throws SQLException;
    }

    public DatabaseOperation()
    {

    }

    private void throwServerError(Exception ex) throws ServerException
    {
        Logger.error(ex.getMessage());
        throw new ServerException(DATABASE_ERROR);
    }

    public T get(Operation<T> operation)
    {
        try
        {
            Session session = SessionSingleton.getSession();

            return operation.tryIt(session);
        }
        catch (Exception ex)
        {
            this.throwServerError(ex);
        }
        return null;
    }

    public T    transaction(TransactionOperation<T> operation) throws ServerException
    {
        T           obj;
        Transaction transaction = null;

        try
        {
            Session session = SessionSingleton.getSession();

            transaction = session.beginTransaction();
            obj = operation.tryIt(session);
            transaction.commit();
            return obj;
        }
        catch (SQLException ex)
        {
            if (transaction != null)
            {
                try
                {
                    transaction.rollback();
                }
                catch (SQLException ex2)
                {
                    this.throwServerError(ex2);
                }
            }
            this.throwServerError(ex);
        }
        catch (Exception ex)
        {
            this.throwServerError(ex);
        }
        return null;
    }
}
