package utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.util.List;

/**
 * Created by chaabane on 26/12/13.
 */
public class Deserializer<T>
{
    private Gson gson;

    public Deserializer()
    {
        gson = new Gson();
    }

    public T     deserialize(String json, Class<T> clazz)
    {
        try
        {
            return gson.fromJson(json, clazz);
        }
        catch (JsonParseException ex)
        {
            return null;
        }
    }

    public List<T> deserializeList(String json, Type type)
    {
        try
        {
            return gson.fromJson(json, type);
        }
        catch (JsonParseException ex)
        {
            return null;
        }
    }
}
