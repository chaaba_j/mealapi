package utils;

import com.google.gson.annotations.Expose;

/**
 * Created by chaabane on 27/12/13.
 */
public class ErrorMessage
{
    @Expose
    private String  message;
    @Expose
    private int     type;


    public ErrorMessage(int type, String message)
    {
        this.setType(type);
        this.setMessage(message);
    }

    public String  serialize()
    {
        Serializer<ErrorMessage> serializer = new Serializer<>();

        return serializer.serialize(this);
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
