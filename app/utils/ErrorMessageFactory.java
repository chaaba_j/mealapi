package utils;

/**
 * Created by chaabane on 27/12/13.
 */
public class ErrorMessageFactory
{
    public static ErrorMessage  create(int type, String message)
    {
        return new ErrorMessage(type, message);
    }
}
