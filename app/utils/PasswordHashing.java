package utils;

import com.google.common.base.Charsets;
import com.google.common.hash.Hashing;

/**
 * Created by chaabane on 27/12/13.
 */
public class PasswordHashing
{
    private static final String SALT = "java";

    public static String hashWithSalt(String password)
    {

        return Hashing.sha1().hashString(password + SALT, Charsets.UTF_8).toString();
    }

    public static String    createHash(String password)
    {
        String  intermediateResult;

        intermediateResult = Hashing.sha1().hashBytes(password.getBytes()).toString();
        return hashWithSalt(intermediateResult);
    }
}
