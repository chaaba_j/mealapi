package utils;

/**
 * Created by chaabane on 27/12/13.
 */
public class RegexUtils
{
    public static boolean   isValidName(String str)
    {
        return str.matches("[a-zA-Z0-9]{3,}[ _]*");
    }

    public static boolean   isValidUrl(String url)
    {
        return url.matches("((http:\\/\\/|https:\\/\\/)?(www.)?(([a-zA-Z0-9-]){2,}\\.){1,4}([a-zA-Z]){2,6}(\\/([a-zA-Z-_\\/\\.0-9#:?=&;,]*)?)?)");
    }
}
