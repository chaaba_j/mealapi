package utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Created by chaabane on 26/12/13.
 */
public class Serializer<T>
{
    private Gson    gson;

    public Serializer()
    {
        gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
    }

    public String serialize(T obj)
    {
        return gson.toJson(obj);
    }
}
