package utils;

import epitech.orm.session.Session;
import epitech.orm.session.SessionFactory;

import java.io.FileNotFoundException;
import java.sql.SQLException;

/**
 * Created by chaabane on 25/12/13.
 */
public class SessionSingleton
{
    private static Session  session = null;


    private SessionSingleton()
    {

    }

    public static Session   getSession() throws FileNotFoundException, ClassNotFoundException, SQLException
    {
        if (session == null)
        {
            session = SessionFactory.create("conf/database.conf.xml");
        }
        return session;
    }

}
