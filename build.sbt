name := "EpiFood"

version := "1.0-SNAPSHOT"

libraryDependencies ++= Seq(
  javaJdbc,
  javaEbean,
  cache
)

libraryDependencies ++= Seq(
  "jdom" % "jdom" % "1.1",
  "jaxen" % "jaxen" % "1.1.4",
  "mysql" % "mysql-connector-java" % "5.1.27",
  "com.google.code.gson" % "gson" % "2.2.4",
  "commons-lang" % "commons-lang" % "2.6",
  "com.google.guava" % "guava" % "r09"
)

play.Project.playJavaSettings
