// @SOURCE:C:/Users/jalal/workspace/mealapi/conf/routes
// @HASH:3b6e095825ee9a3a5a0ef619aa9f264168290d41
// @DATE:Mon Jan 06 00:00:22 CET 2014

package controllers;

public class routes {
public static final controllers.ReverseCountryController CountryController = new controllers.ReverseCountryController();
public static final controllers.ReverseMealController MealController = new controllers.ReverseMealController();
public static final controllers.ReverseCategoryController CategoryController = new controllers.ReverseCategoryController();
public static final controllers.ReverseUserController UserController = new controllers.ReverseUserController();
public static final controllers.ReverseChallengeController ChallengeController = new controllers.ReverseChallengeController();
public static class javascript {
public static final controllers.javascript.ReverseCountryController CountryController = new controllers.javascript.ReverseCountryController();
public static final controllers.javascript.ReverseMealController MealController = new controllers.javascript.ReverseMealController();
public static final controllers.javascript.ReverseCategoryController CategoryController = new controllers.javascript.ReverseCategoryController();
public static final controllers.javascript.ReverseUserController UserController = new controllers.javascript.ReverseUserController();
public static final controllers.javascript.ReverseChallengeController ChallengeController = new controllers.javascript.ReverseChallengeController();
}
public static class ref {
public static final controllers.ref.ReverseCountryController CountryController = new controllers.ref.ReverseCountryController();
public static final controllers.ref.ReverseMealController MealController = new controllers.ref.ReverseMealController();
public static final controllers.ref.ReverseCategoryController CategoryController = new controllers.ref.ReverseCategoryController();
public static final controllers.ref.ReverseUserController UserController = new controllers.ref.ReverseUserController();
public static final controllers.ref.ReverseChallengeController ChallengeController = new controllers.ref.ReverseChallengeController();
}
}
          