// @SOURCE:C:/Users/jalal/workspace/mealapi/conf/routes
// @HASH:3b6e095825ee9a3a5a0ef619aa9f264168290d41
// @DATE:Mon Jan 06 00:00:22 CET 2014

import Routes.{prefix => _prefix, defaultPrefix => _defaultPrefix}
import play.core._
import play.core.Router._
import play.core.j._

import play.api.mvc._
import play.libs.F

import Router.queryString


// @LINE:49
// @LINE:48
// @LINE:47
// @LINE:46
// @LINE:45
// @LINE:34
// @LINE:33
// @LINE:32
// @LINE:31
// @LINE:30
// @LINE:26
// @LINE:25
// @LINE:24
// @LINE:23
// @LINE:22
// @LINE:18
// @LINE:17
// @LINE:16
// @LINE:15
// @LINE:14
// @LINE:13
// @LINE:12
// @LINE:11
// @LINE:10
// @LINE:7
package controllers {

// @LINE:34
// @LINE:33
// @LINE:32
// @LINE:31
// @LINE:30
class ReverseCountryController {
    

// @LINE:34
def delete(id:Long): Call = {
   Call("DELETE", _prefix + { _defaultPrefix } + "country/" + implicitly[PathBindable[Long]].unbind("id", id))
}
                                                

// @LINE:31
def show(id:Long): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "country/" + implicitly[PathBindable[Long]].unbind("id", id))
}
                                                

// @LINE:32
def add(): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "country")
}
                                                

// @LINE:30
def list(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "country")
}
                                                

// @LINE:33
def update(): Call = {
   Call("PUT", _prefix + { _defaultPrefix } + "country")
}
                                                
    
}
                          

// @LINE:18
// @LINE:17
// @LINE:16
// @LINE:15
// @LINE:14
// @LINE:13
// @LINE:12
// @LINE:11
// @LINE:10
class ReverseMealController {
    

// @LINE:14
def listByCountry(country_id:Long): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "meal/search/country/" + implicitly[PathBindable[Long]].unbind("country_id", country_id))
}
                                                

// @LINE:18
def delete(id:Long): Call = {
   Call("DELETE", _prefix + { _defaultPrefix } + "meal/" + implicitly[PathBindable[Long]].unbind("id", id))
}
                                                

// @LINE:11
def show(id:Long): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "meal/" + implicitly[PathBindable[Long]].unbind("id", id))
}
                                                

// @LINE:16
def add(): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "meal")
}
                                                

// @LINE:10
def list(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "meal")
}
                                                

// @LINE:15
def listByUser(user_id:Long): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "meal/search/user/" + implicitly[PathBindable[Long]].unbind("user_id", user_id))
}
                                                

// @LINE:12
def listByCategory(category_id:Long): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "meal/search/category/" + implicitly[PathBindable[Long]].unbind("category_id", category_id))
}
                                                

// @LINE:17
def update(): Call = {
   Call("PUT", _prefix + { _defaultPrefix } + "meal")
}
                                                

// @LINE:13
def listByRate(rate:Int): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "meal/search/rate/" + implicitly[PathBindable[Int]].unbind("rate", rate))
}
                                                
    
}
                          

// @LINE:26
// @LINE:25
// @LINE:24
// @LINE:23
// @LINE:22
class ReverseCategoryController {
    

// @LINE:26
def delete(id:Long): Call = {
   Call("DELETE", _prefix + { _defaultPrefix } + "category/" + implicitly[PathBindable[Long]].unbind("id", id))
}
                                                

// @LINE:23
def show(id:Long): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "category/" + implicitly[PathBindable[Long]].unbind("id", id))
}
                                                

// @LINE:24
def add(): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "category")
}
                                                

// @LINE:22
def list(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "category")
}
                                                

// @LINE:25
def update(): Call = {
   Call("PUT", _prefix + { _defaultPrefix } + "category")
}
                                                
    
}
                          

// @LINE:49
// @LINE:48
// @LINE:47
// @LINE:46
// @LINE:45
class ReverseUserController {
    

// @LINE:49
def delete(id:Long): Call = {
   Call("DELETE", _prefix + { _defaultPrefix } + "users/" + implicitly[PathBindable[Long]].unbind("id", id))
}
                                                

// @LINE:46
def show(id:Long): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "users/" + implicitly[PathBindable[Long]].unbind("id", id))
}
                                                

// @LINE:47
def register(): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "users")
}
                                                

// @LINE:45
def list(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "users")
}
                                                

// @LINE:48
def update(): Call = {
   Call("PUT", _prefix + { _defaultPrefix } + "users")
}
                                                
    
}
                          

// @LINE:7
class ReverseChallengeController {
    

// @LINE:7
def challenge(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "challenge")
}
                                                
    
}
                          
}
                  


// @LINE:49
// @LINE:48
// @LINE:47
// @LINE:46
// @LINE:45
// @LINE:34
// @LINE:33
// @LINE:32
// @LINE:31
// @LINE:30
// @LINE:26
// @LINE:25
// @LINE:24
// @LINE:23
// @LINE:22
// @LINE:18
// @LINE:17
// @LINE:16
// @LINE:15
// @LINE:14
// @LINE:13
// @LINE:12
// @LINE:11
// @LINE:10
// @LINE:7
package controllers.javascript {

// @LINE:34
// @LINE:33
// @LINE:32
// @LINE:31
// @LINE:30
class ReverseCountryController {
    

// @LINE:34
def delete : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.CountryController.delete",
   """
      function(id) {
      return _wA({method:"DELETE", url:"""" + _prefix + { _defaultPrefix } + """" + "country/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("id", id)})
      }
   """
)
                        

// @LINE:31
def show : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.CountryController.show",
   """
      function(id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "country/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("id", id)})
      }
   """
)
                        

// @LINE:32
def add : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.CountryController.add",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "country"})
      }
   """
)
                        

// @LINE:30
def list : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.CountryController.list",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "country"})
      }
   """
)
                        

// @LINE:33
def update : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.CountryController.update",
   """
      function() {
      return _wA({method:"PUT", url:"""" + _prefix + { _defaultPrefix } + """" + "country"})
      }
   """
)
                        
    
}
              

// @LINE:18
// @LINE:17
// @LINE:16
// @LINE:15
// @LINE:14
// @LINE:13
// @LINE:12
// @LINE:11
// @LINE:10
class ReverseMealController {
    

// @LINE:14
def listByCountry : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.MealController.listByCountry",
   """
      function(country_id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "meal/search/country/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("country_id", country_id)})
      }
   """
)
                        

// @LINE:18
def delete : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.MealController.delete",
   """
      function(id) {
      return _wA({method:"DELETE", url:"""" + _prefix + { _defaultPrefix } + """" + "meal/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("id", id)})
      }
   """
)
                        

// @LINE:11
def show : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.MealController.show",
   """
      function(id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "meal/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("id", id)})
      }
   """
)
                        

// @LINE:16
def add : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.MealController.add",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "meal"})
      }
   """
)
                        

// @LINE:10
def list : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.MealController.list",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "meal"})
      }
   """
)
                        

// @LINE:15
def listByUser : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.MealController.listByUser",
   """
      function(user_id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "meal/search/user/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("user_id", user_id)})
      }
   """
)
                        

// @LINE:12
def listByCategory : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.MealController.listByCategory",
   """
      function(category_id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "meal/search/category/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("category_id", category_id)})
      }
   """
)
                        

// @LINE:17
def update : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.MealController.update",
   """
      function() {
      return _wA({method:"PUT", url:"""" + _prefix + { _defaultPrefix } + """" + "meal"})
      }
   """
)
                        

// @LINE:13
def listByRate : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.MealController.listByRate",
   """
      function(rate) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "meal/search/rate/" + (""" + implicitly[PathBindable[Int]].javascriptUnbind + """)("rate", rate)})
      }
   """
)
                        
    
}
              

// @LINE:26
// @LINE:25
// @LINE:24
// @LINE:23
// @LINE:22
class ReverseCategoryController {
    

// @LINE:26
def delete : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.CategoryController.delete",
   """
      function(id) {
      return _wA({method:"DELETE", url:"""" + _prefix + { _defaultPrefix } + """" + "category/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("id", id)})
      }
   """
)
                        

// @LINE:23
def show : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.CategoryController.show",
   """
      function(id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "category/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("id", id)})
      }
   """
)
                        

// @LINE:24
def add : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.CategoryController.add",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "category"})
      }
   """
)
                        

// @LINE:22
def list : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.CategoryController.list",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "category"})
      }
   """
)
                        

// @LINE:25
def update : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.CategoryController.update",
   """
      function() {
      return _wA({method:"PUT", url:"""" + _prefix + { _defaultPrefix } + """" + "category"})
      }
   """
)
                        
    
}
              

// @LINE:49
// @LINE:48
// @LINE:47
// @LINE:46
// @LINE:45
class ReverseUserController {
    

// @LINE:49
def delete : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.UserController.delete",
   """
      function(id) {
      return _wA({method:"DELETE", url:"""" + _prefix + { _defaultPrefix } + """" + "users/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("id", id)})
      }
   """
)
                        

// @LINE:46
def show : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.UserController.show",
   """
      function(id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "users/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("id", id)})
      }
   """
)
                        

// @LINE:47
def register : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.UserController.register",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "users"})
      }
   """
)
                        

// @LINE:45
def list : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.UserController.list",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "users"})
      }
   """
)
                        

// @LINE:48
def update : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.UserController.update",
   """
      function() {
      return _wA({method:"PUT", url:"""" + _prefix + { _defaultPrefix } + """" + "users"})
      }
   """
)
                        
    
}
              

// @LINE:7
class ReverseChallengeController {
    

// @LINE:7
def challenge : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.ChallengeController.challenge",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "challenge"})
      }
   """
)
                        
    
}
              
}
        


// @LINE:49
// @LINE:48
// @LINE:47
// @LINE:46
// @LINE:45
// @LINE:34
// @LINE:33
// @LINE:32
// @LINE:31
// @LINE:30
// @LINE:26
// @LINE:25
// @LINE:24
// @LINE:23
// @LINE:22
// @LINE:18
// @LINE:17
// @LINE:16
// @LINE:15
// @LINE:14
// @LINE:13
// @LINE:12
// @LINE:11
// @LINE:10
// @LINE:7
package controllers.ref {


// @LINE:34
// @LINE:33
// @LINE:32
// @LINE:31
// @LINE:30
class ReverseCountryController {
    

// @LINE:34
def delete(id:Long): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.CountryController.delete(id), HandlerDef(this, "controllers.CountryController", "delete", Seq(classOf[Long]), "DELETE", """""", _prefix + """country/$id<[^/]+>""")
)
                      

// @LINE:31
def show(id:Long): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.CountryController.show(id), HandlerDef(this, "controllers.CountryController", "show", Seq(classOf[Long]), "GET", """""", _prefix + """country/$id<[^/]+>""")
)
                      

// @LINE:32
def add(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.CountryController.add(), HandlerDef(this, "controllers.CountryController", "add", Seq(), "POST", """""", _prefix + """country""")
)
                      

// @LINE:30
def list(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.CountryController.list(), HandlerDef(this, "controllers.CountryController", "list", Seq(), "GET", """""", _prefix + """country""")
)
                      

// @LINE:33
def update(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.CountryController.update(), HandlerDef(this, "controllers.CountryController", "update", Seq(), "PUT", """""", _prefix + """country""")
)
                      
    
}
                          

// @LINE:18
// @LINE:17
// @LINE:16
// @LINE:15
// @LINE:14
// @LINE:13
// @LINE:12
// @LINE:11
// @LINE:10
class ReverseMealController {
    

// @LINE:14
def listByCountry(country_id:Long): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.MealController.listByCountry(country_id), HandlerDef(this, "controllers.MealController", "listByCountry", Seq(classOf[Long]), "GET", """""", _prefix + """meal/search/country/$country_id<[^/]+>""")
)
                      

// @LINE:18
def delete(id:Long): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.MealController.delete(id), HandlerDef(this, "controllers.MealController", "delete", Seq(classOf[Long]), "DELETE", """""", _prefix + """meal/$id<[^/]+>""")
)
                      

// @LINE:11
def show(id:Long): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.MealController.show(id), HandlerDef(this, "controllers.MealController", "show", Seq(classOf[Long]), "GET", """""", _prefix + """meal/$id<[^/]+>""")
)
                      

// @LINE:16
def add(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.MealController.add(), HandlerDef(this, "controllers.MealController", "add", Seq(), "POST", """""", _prefix + """meal""")
)
                      

// @LINE:10
def list(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.MealController.list(), HandlerDef(this, "controllers.MealController", "list", Seq(), "GET", """ Meal API""", _prefix + """meal""")
)
                      

// @LINE:15
def listByUser(user_id:Long): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.MealController.listByUser(user_id), HandlerDef(this, "controllers.MealController", "listByUser", Seq(classOf[Long]), "GET", """""", _prefix + """meal/search/user/$user_id<[^/]+>""")
)
                      

// @LINE:12
def listByCategory(category_id:Long): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.MealController.listByCategory(category_id), HandlerDef(this, "controllers.MealController", "listByCategory", Seq(classOf[Long]), "GET", """""", _prefix + """meal/search/category/$category_id<[^/]+>""")
)
                      

// @LINE:17
def update(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.MealController.update(), HandlerDef(this, "controllers.MealController", "update", Seq(), "PUT", """""", _prefix + """meal""")
)
                      

// @LINE:13
def listByRate(rate:Int): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.MealController.listByRate(rate), HandlerDef(this, "controllers.MealController", "listByRate", Seq(classOf[Int]), "GET", """""", _prefix + """meal/search/rate/$rate<[^/]+>""")
)
                      
    
}
                          

// @LINE:26
// @LINE:25
// @LINE:24
// @LINE:23
// @LINE:22
class ReverseCategoryController {
    

// @LINE:26
def delete(id:Long): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.CategoryController.delete(id), HandlerDef(this, "controllers.CategoryController", "delete", Seq(classOf[Long]), "DELETE", """""", _prefix + """category/$id<[^/]+>""")
)
                      

// @LINE:23
def show(id:Long): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.CategoryController.show(id), HandlerDef(this, "controllers.CategoryController", "show", Seq(classOf[Long]), "GET", """""", _prefix + """category/$id<[^/]+>""")
)
                      

// @LINE:24
def add(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.CategoryController.add(), HandlerDef(this, "controllers.CategoryController", "add", Seq(), "POST", """""", _prefix + """category""")
)
                      

// @LINE:22
def list(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.CategoryController.list(), HandlerDef(this, "controllers.CategoryController", "list", Seq(), "GET", """ Category API""", _prefix + """category""")
)
                      

// @LINE:25
def update(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.CategoryController.update(), HandlerDef(this, "controllers.CategoryController", "update", Seq(), "PUT", """""", _prefix + """category""")
)
                      
    
}
                          

// @LINE:49
// @LINE:48
// @LINE:47
// @LINE:46
// @LINE:45
class ReverseUserController {
    

// @LINE:49
def delete(id:Long): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.UserController.delete(id), HandlerDef(this, "controllers.UserController", "delete", Seq(classOf[Long]), "DELETE", """""", _prefix + """users/$id<[^/]+>""")
)
                      

// @LINE:46
def show(id:Long): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.UserController.show(id), HandlerDef(this, "controllers.UserController", "show", Seq(classOf[Long]), "GET", """""", _prefix + """users/$id<[^/]+>""")
)
                      

// @LINE:47
def register(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.UserController.register(), HandlerDef(this, "controllers.UserController", "register", Seq(), "POST", """""", _prefix + """users""")
)
                      

// @LINE:45
def list(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.UserController.list(), HandlerDef(this, "controllers.UserController", "list", Seq(), "GET", """""", _prefix + """users""")
)
                      

// @LINE:48
def update(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.UserController.update(), HandlerDef(this, "controllers.UserController", "update", Seq(), "PUT", """""", _prefix + """users""")
)
                      
    
}
                          

// @LINE:7
class ReverseChallengeController {
    

// @LINE:7
def challenge(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.ChallengeController.challenge(), HandlerDef(this, "controllers.ChallengeController", "challenge", Seq(), "GET", """""", _prefix + """challenge""")
)
                      
    
}
                          
}
        
    