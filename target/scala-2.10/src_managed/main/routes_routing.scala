// @SOURCE:C:/Users/jalal/workspace/mealapi/conf/routes
// @HASH:3b6e095825ee9a3a5a0ef619aa9f264168290d41
// @DATE:Mon Jan 06 00:00:22 CET 2014


import play.core._
import play.core.Router._
import play.core.j._

import play.api.mvc._
import play.libs.F

import Router.queryString

object Routes extends Router.Routes {

private var _prefix = "/"

def setPrefix(prefix: String) {
  _prefix = prefix
  List[(String,Routes)]().foreach {
    case (p, router) => router.setPrefix(prefix + (if(prefix.endsWith("/")) "" else "/") + p)
  }
}

def prefix = _prefix

lazy val defaultPrefix = { if(Routes.prefix.endsWith("/")) "" else "/" }


// @LINE:7
private[this] lazy val controllers_ChallengeController_challenge0 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("challenge"))))
        

// @LINE:10
private[this] lazy val controllers_MealController_list1 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("meal"))))
        

// @LINE:11
private[this] lazy val controllers_MealController_show2 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("meal/"),DynamicPart("id", """[^/]+""",true))))
        

// @LINE:12
private[this] lazy val controllers_MealController_listByCategory3 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("meal/search/category/"),DynamicPart("category_id", """[^/]+""",true))))
        

// @LINE:13
private[this] lazy val controllers_MealController_listByRate4 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("meal/search/rate/"),DynamicPart("rate", """[^/]+""",true))))
        

// @LINE:14
private[this] lazy val controllers_MealController_listByCountry5 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("meal/search/country/"),DynamicPart("country_id", """[^/]+""",true))))
        

// @LINE:15
private[this] lazy val controllers_MealController_listByUser6 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("meal/search/user/"),DynamicPart("user_id", """[^/]+""",true))))
        

// @LINE:16
private[this] lazy val controllers_MealController_add7 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("meal"))))
        

// @LINE:17
private[this] lazy val controllers_MealController_update8 = Route("PUT", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("meal"))))
        

// @LINE:18
private[this] lazy val controllers_MealController_delete9 = Route("DELETE", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("meal/"),DynamicPart("id", """[^/]+""",true))))
        

// @LINE:22
private[this] lazy val controllers_CategoryController_list10 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("category"))))
        

// @LINE:23
private[this] lazy val controllers_CategoryController_show11 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("category/"),DynamicPart("id", """[^/]+""",true))))
        

// @LINE:24
private[this] lazy val controllers_CategoryController_add12 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("category"))))
        

// @LINE:25
private[this] lazy val controllers_CategoryController_update13 = Route("PUT", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("category"))))
        

// @LINE:26
private[this] lazy val controllers_CategoryController_delete14 = Route("DELETE", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("category/"),DynamicPart("id", """[^/]+""",true))))
        

// @LINE:30
private[this] lazy val controllers_CountryController_list15 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("country"))))
        

// @LINE:31
private[this] lazy val controllers_CountryController_show16 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("country/"),DynamicPart("id", """[^/]+""",true))))
        

// @LINE:32
private[this] lazy val controllers_CountryController_add17 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("country"))))
        

// @LINE:33
private[this] lazy val controllers_CountryController_update18 = Route("PUT", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("country"))))
        

// @LINE:34
private[this] lazy val controllers_CountryController_delete19 = Route("DELETE", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("country/"),DynamicPart("id", """[^/]+""",true))))
        

// @LINE:45
private[this] lazy val controllers_UserController_list20 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("users"))))
        

// @LINE:46
private[this] lazy val controllers_UserController_show21 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("users/"),DynamicPart("id", """[^/]+""",true))))
        

// @LINE:47
private[this] lazy val controllers_UserController_register22 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("users"))))
        

// @LINE:48
private[this] lazy val controllers_UserController_update23 = Route("PUT", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("users"))))
        

// @LINE:49
private[this] lazy val controllers_UserController_delete24 = Route("DELETE", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("users/"),DynamicPart("id", """[^/]+""",true))))
        
def documentation = List(("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """challenge""","""controllers.ChallengeController.challenge()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """meal""","""controllers.MealController.list()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """meal/$id<[^/]+>""","""controllers.MealController.show(id:Long)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """meal/search/category/$category_id<[^/]+>""","""controllers.MealController.listByCategory(category_id:Long)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """meal/search/rate/$rate<[^/]+>""","""controllers.MealController.listByRate(rate:Int)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """meal/search/country/$country_id<[^/]+>""","""controllers.MealController.listByCountry(country_id:Long)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """meal/search/user/$user_id<[^/]+>""","""controllers.MealController.listByUser(user_id:Long)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """meal""","""controllers.MealController.add()"""),("""PUT""", prefix + (if(prefix.endsWith("/")) "" else "/") + """meal""","""controllers.MealController.update()"""),("""DELETE""", prefix + (if(prefix.endsWith("/")) "" else "/") + """meal/$id<[^/]+>""","""controllers.MealController.delete(id:Long)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """category""","""controllers.CategoryController.list()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """category/$id<[^/]+>""","""controllers.CategoryController.show(id:Long)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """category""","""controllers.CategoryController.add()"""),("""PUT""", prefix + (if(prefix.endsWith("/")) "" else "/") + """category""","""controllers.CategoryController.update()"""),("""DELETE""", prefix + (if(prefix.endsWith("/")) "" else "/") + """category/$id<[^/]+>""","""controllers.CategoryController.delete(id:Long)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """country""","""controllers.CountryController.list()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """country/$id<[^/]+>""","""controllers.CountryController.show(id:Long)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """country""","""controllers.CountryController.add()"""),("""PUT""", prefix + (if(prefix.endsWith("/")) "" else "/") + """country""","""controllers.CountryController.update()"""),("""DELETE""", prefix + (if(prefix.endsWith("/")) "" else "/") + """country/$id<[^/]+>""","""controllers.CountryController.delete(id:Long)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """users""","""controllers.UserController.list()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """users/$id<[^/]+>""","""controllers.UserController.show(id:Long)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """users""","""controllers.UserController.register()"""),("""PUT""", prefix + (if(prefix.endsWith("/")) "" else "/") + """users""","""controllers.UserController.update()"""),("""DELETE""", prefix + (if(prefix.endsWith("/")) "" else "/") + """users/$id<[^/]+>""","""controllers.UserController.delete(id:Long)""")).foldLeft(List.empty[(String,String,String)]) { (s,e) => e.asInstanceOf[Any] match {
  case r @ (_,_,_) => s :+ r.asInstanceOf[(String,String,String)]
  case l => s ++ l.asInstanceOf[List[(String,String,String)]] 
}}
      

def routes:PartialFunction[RequestHeader,Handler] = {

// @LINE:7
case controllers_ChallengeController_challenge0(params) => {
   call { 
        invokeHandler(controllers.ChallengeController.challenge(), HandlerDef(this, "controllers.ChallengeController", "challenge", Nil,"GET", """""", Routes.prefix + """challenge"""))
   }
}
        

// @LINE:10
case controllers_MealController_list1(params) => {
   call { 
        invokeHandler(controllers.MealController.list(), HandlerDef(this, "controllers.MealController", "list", Nil,"GET", """ Meal API""", Routes.prefix + """meal"""))
   }
}
        

// @LINE:11
case controllers_MealController_show2(params) => {
   call(params.fromPath[Long]("id", None)) { (id) =>
        invokeHandler(controllers.MealController.show(id), HandlerDef(this, "controllers.MealController", "show", Seq(classOf[Long]),"GET", """""", Routes.prefix + """meal/$id<[^/]+>"""))
   }
}
        

// @LINE:12
case controllers_MealController_listByCategory3(params) => {
   call(params.fromPath[Long]("category_id", None)) { (category_id) =>
        invokeHandler(controllers.MealController.listByCategory(category_id), HandlerDef(this, "controllers.MealController", "listByCategory", Seq(classOf[Long]),"GET", """""", Routes.prefix + """meal/search/category/$category_id<[^/]+>"""))
   }
}
        

// @LINE:13
case controllers_MealController_listByRate4(params) => {
   call(params.fromPath[Int]("rate", None)) { (rate) =>
        invokeHandler(controllers.MealController.listByRate(rate), HandlerDef(this, "controllers.MealController", "listByRate", Seq(classOf[Int]),"GET", """""", Routes.prefix + """meal/search/rate/$rate<[^/]+>"""))
   }
}
        

// @LINE:14
case controllers_MealController_listByCountry5(params) => {
   call(params.fromPath[Long]("country_id", None)) { (country_id) =>
        invokeHandler(controllers.MealController.listByCountry(country_id), HandlerDef(this, "controllers.MealController", "listByCountry", Seq(classOf[Long]),"GET", """""", Routes.prefix + """meal/search/country/$country_id<[^/]+>"""))
   }
}
        

// @LINE:15
case controllers_MealController_listByUser6(params) => {
   call(params.fromPath[Long]("user_id", None)) { (user_id) =>
        invokeHandler(controllers.MealController.listByUser(user_id), HandlerDef(this, "controllers.MealController", "listByUser", Seq(classOf[Long]),"GET", """""", Routes.prefix + """meal/search/user/$user_id<[^/]+>"""))
   }
}
        

// @LINE:16
case controllers_MealController_add7(params) => {
   call { 
        invokeHandler(controllers.MealController.add(), HandlerDef(this, "controllers.MealController", "add", Nil,"POST", """""", Routes.prefix + """meal"""))
   }
}
        

// @LINE:17
case controllers_MealController_update8(params) => {
   call { 
        invokeHandler(controllers.MealController.update(), HandlerDef(this, "controllers.MealController", "update", Nil,"PUT", """""", Routes.prefix + """meal"""))
   }
}
        

// @LINE:18
case controllers_MealController_delete9(params) => {
   call(params.fromPath[Long]("id", None)) { (id) =>
        invokeHandler(controllers.MealController.delete(id), HandlerDef(this, "controllers.MealController", "delete", Seq(classOf[Long]),"DELETE", """""", Routes.prefix + """meal/$id<[^/]+>"""))
   }
}
        

// @LINE:22
case controllers_CategoryController_list10(params) => {
   call { 
        invokeHandler(controllers.CategoryController.list(), HandlerDef(this, "controllers.CategoryController", "list", Nil,"GET", """ Category API""", Routes.prefix + """category"""))
   }
}
        

// @LINE:23
case controllers_CategoryController_show11(params) => {
   call(params.fromPath[Long]("id", None)) { (id) =>
        invokeHandler(controllers.CategoryController.show(id), HandlerDef(this, "controllers.CategoryController", "show", Seq(classOf[Long]),"GET", """""", Routes.prefix + """category/$id<[^/]+>"""))
   }
}
        

// @LINE:24
case controllers_CategoryController_add12(params) => {
   call { 
        invokeHandler(controllers.CategoryController.add(), HandlerDef(this, "controllers.CategoryController", "add", Nil,"POST", """""", Routes.prefix + """category"""))
   }
}
        

// @LINE:25
case controllers_CategoryController_update13(params) => {
   call { 
        invokeHandler(controllers.CategoryController.update(), HandlerDef(this, "controllers.CategoryController", "update", Nil,"PUT", """""", Routes.prefix + """category"""))
   }
}
        

// @LINE:26
case controllers_CategoryController_delete14(params) => {
   call(params.fromPath[Long]("id", None)) { (id) =>
        invokeHandler(controllers.CategoryController.delete(id), HandlerDef(this, "controllers.CategoryController", "delete", Seq(classOf[Long]),"DELETE", """""", Routes.prefix + """category/$id<[^/]+>"""))
   }
}
        

// @LINE:30
case controllers_CountryController_list15(params) => {
   call { 
        invokeHandler(controllers.CountryController.list(), HandlerDef(this, "controllers.CountryController", "list", Nil,"GET", """""", Routes.prefix + """country"""))
   }
}
        

// @LINE:31
case controllers_CountryController_show16(params) => {
   call(params.fromPath[Long]("id", None)) { (id) =>
        invokeHandler(controllers.CountryController.show(id), HandlerDef(this, "controllers.CountryController", "show", Seq(classOf[Long]),"GET", """""", Routes.prefix + """country/$id<[^/]+>"""))
   }
}
        

// @LINE:32
case controllers_CountryController_add17(params) => {
   call { 
        invokeHandler(controllers.CountryController.add(), HandlerDef(this, "controllers.CountryController", "add", Nil,"POST", """""", Routes.prefix + """country"""))
   }
}
        

// @LINE:33
case controllers_CountryController_update18(params) => {
   call { 
        invokeHandler(controllers.CountryController.update(), HandlerDef(this, "controllers.CountryController", "update", Nil,"PUT", """""", Routes.prefix + """country"""))
   }
}
        

// @LINE:34
case controllers_CountryController_delete19(params) => {
   call(params.fromPath[Long]("id", None)) { (id) =>
        invokeHandler(controllers.CountryController.delete(id), HandlerDef(this, "controllers.CountryController", "delete", Seq(classOf[Long]),"DELETE", """""", Routes.prefix + """country/$id<[^/]+>"""))
   }
}
        

// @LINE:45
case controllers_UserController_list20(params) => {
   call { 
        invokeHandler(controllers.UserController.list(), HandlerDef(this, "controllers.UserController", "list", Nil,"GET", """""", Routes.prefix + """users"""))
   }
}
        

// @LINE:46
case controllers_UserController_show21(params) => {
   call(params.fromPath[Long]("id", None)) { (id) =>
        invokeHandler(controllers.UserController.show(id), HandlerDef(this, "controllers.UserController", "show", Seq(classOf[Long]),"GET", """""", Routes.prefix + """users/$id<[^/]+>"""))
   }
}
        

// @LINE:47
case controllers_UserController_register22(params) => {
   call { 
        invokeHandler(controllers.UserController.register(), HandlerDef(this, "controllers.UserController", "register", Nil,"POST", """""", Routes.prefix + """users"""))
   }
}
        

// @LINE:48
case controllers_UserController_update23(params) => {
   call { 
        invokeHandler(controllers.UserController.update(), HandlerDef(this, "controllers.UserController", "update", Nil,"PUT", """""", Routes.prefix + """users"""))
   }
}
        

// @LINE:49
case controllers_UserController_delete24(params) => {
   call(params.fromPath[Long]("id", None)) { (id) =>
        invokeHandler(controllers.UserController.delete(id), HandlerDef(this, "controllers.UserController", "delete", Seq(classOf[Long]),"DELETE", """""", Routes.prefix + """users/$id<[^/]+>"""))
   }
}
        
}

}
     