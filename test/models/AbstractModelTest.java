package models;


import domains.AbstractObject;
import domains.User;
import exceptions.InvalidInputException;
import exceptions.ObjectNotFoundException;
import exceptions.ServerException;
import junit.framework.TestCase;
import utils.Deserializer;
import utils.Serializer;

import java.lang.reflect.Type;
import java.util.List;

/**
 * Created by chaabane on 01/01/14.
 */
public class AbstractModelTest<T extends AbstractObject> extends TestCase
{
    protected AbstractModel<T>  model;
    private Class<T>            clazz;
    protected User              user;

    protected interface AssertFunction<T>
    {
        public void assertEquals(T expected, T obj);
    }

    protected interface AssertListFunction<T>
    {
        public void assertEquals(List<T> expected, List<T> objList);
    }

    public AbstractModelTest(AbstractModel<T> model, Class<T> clazz)
    {
        User        user = new User();

        user.setUsername("marcham");
        user.setUserType(User.ADMIN);
        this.model = model;
        this.clazz = clazz;
        this.user = user;
    }


    protected void addTest(T obj, AssertFunction<T> assertFunction) throws InvalidInputException, ServerException
    {
        T               savedObj;
        String          json;
        Serializer<T>   serializer = new Serializer<>();
        Deserializer<T> deserializer = new Deserializer<>();

        json = model.add(user, serializer.serialize(obj));
        savedObj = deserializer.deserialize(json, clazz);
        assertFunction.assertEquals(obj, savedObj);
    }

    protected void updateTest(T obj, AssertFunction<T> assertFunction) throws InvalidInputException, ServerException, ObjectNotFoundException
    {
        T               updatedObj;
        String          json;
        Serializer<T>   serializer = new Serializer<>();
        Deserializer<T> deserializer = new Deserializer<>();

        json = model.update(user, serializer.serialize(obj));
        updatedObj = deserializer.deserialize(json, clazz);
        assertFunction.assertEquals(obj, updatedObj);
    }

    protected void deleteTest(T obj, AssertFunction<T> assertFunction) throws ServerException, ObjectNotFoundException
    {
        T               deletedObj;
        String          json;
        Deserializer<T> deserializer = new Deserializer<>();

        json = model.delete(user, obj.getId());
        deletedObj = deserializer.deserialize(json, clazz);
        assertFunction.assertEquals(obj, deletedObj);
    }

    protected void  getAllTest(List<T> objListExpected, Type type, AssertListFunction<T> assertFunction) throws ServerException
    {
        String          json;
        Deserializer<T> deserializer = new Deserializer<>();
        List<T>         objs;

        json = model.getAll();
        objs = deserializer.deserializeList(json, type);
        assertFunction.assertEquals(objListExpected, objs);
    }

    protected void  getById(T obj, AssertFunction<T> assertFunction) throws ObjectNotFoundException, ServerException
    {
        Deserializer<T> deserializer = new Deserializer<>();
        T               gettedObj;
        String          json;

        json = model.getById(obj.getId());
        gettedObj = deserializer.deserialize(json, clazz);
        assertFunction.assertEquals(obj, gettedObj);
    }
}
