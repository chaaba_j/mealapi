package utils;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by chaabane on 31/12/13.
 */
public class PasswordHashingTest
{
    @Test
    public void testHash()
    {
        String  toBeHashed = "toto";

        Assert.assertEquals(PasswordHashing.createHash(toBeHashed), "a7dacf5800abbf9703d125db3d5739d73b381a93");
    }
}
