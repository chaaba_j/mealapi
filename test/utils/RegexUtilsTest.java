package utils;

import junit.framework.Assert;
import org.junit.Test;

/**
 * Created by chaabane on 31/12/13.
 */
public class RegexUtilsTest
{
    @Test
    public void testWithValidName()
    {
        Assert.assertTrue(RegexUtils.isValidName("toto"));
    }

    @Test
    public void testWithValidUrl()
    {
        Assert.assertTrue(RegexUtils.isValidUrl("http://wwww.google.fr"));
    }

    @Test
    public void testWithInvalidName()
    {
        Assert.assertFalse(RegexUtils.isValidName("t$^ùù"));
    }

    @Test
    public void testWithInvalidUrl()
    {
        Assert.assertFalse(RegexUtils.isValidUrl("blablabla"));
    }
}
